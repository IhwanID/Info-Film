package me.ihwan.infofilm.network

import me.ihwan.infofilm.BuildConfig.API_KEY
import me.ihwan.infofilm.BuildConfig.BASE_URL

object TMDBApi {

    fun getMovie(): String {
        return BASE_URL + API_KEY
    }
}