package me.ihwan.infofilm.main

import android.support.v7.widget.RecyclerView
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.TextView
import com.squareup.picasso.Picasso
import me.ihwan.infofilm.BuildConfig.URL_POSTER
import me.ihwan.infofilm.R.id.movie_poster
import me.ihwan.infofilm.R.id.movie_title
import me.ihwan.infofilm.model.Movie
import org.jetbrains.anko.*
import org.jetbrains.anko.sdk25.coroutines.onClick

class MainAdapter(private val results: List<Movie>, private val listener: (Movie) -> Unit)
    : RecyclerView.Adapter<MovieViewHolder>(){

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MovieViewHolder {
        return MovieViewHolder(MovieUI().createView(AnkoContext.create(parent.context, parent)))
    }

    override fun getItemCount(): Int = results.size

    override fun onBindViewHolder(p0: MovieViewHolder, p1: Int) {
        p0.bindItem(results[p1], listener)
    }
}

class MovieUI : AnkoComponent<ViewGroup> {
    override fun createView(ui: AnkoContext<ViewGroup>): View {
        return with(ui) {
            linearLayout {
                lparams(width = matchParent, height = wrapContent)
                padding = dip(5)
                orientation = LinearLayout.VERTICAL

                imageView {
                    id = movie_poster
                }.lparams{
                    height = dip(250)
                    width = wrapContent
                }

                textView {
                    id = movie_title
                    textSize = 16f
                }.lparams{
                    margin = dip(15)
                }

            }
        }
    }

}

class MovieViewHolder(view: View) : RecyclerView.ViewHolder(view){

    private val moviePoster: ImageView = view.find(movie_poster)
    private val movieTitle: TextView = view.find(movie_title)

    fun bindItem(movies: Movie, listener: (Movie) -> Unit) {

        Picasso.get().load(URL_POSTER + movies.poster).into(moviePoster)
        movieTitle.text = movies.title

        moviePoster.onClick {
            listener(movies)
        }
    }
}