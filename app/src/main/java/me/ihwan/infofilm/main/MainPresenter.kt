package me.ihwan.infofilm.main

import com.google.gson.Gson
import me.ihwan.infofilm.model.MovieResponse
import me.ihwan.infofilm.network.ApiRepository
import me.ihwan.infofilm.network.TMDBApi
import org.jetbrains.anko.doAsync
import org.jetbrains.anko.uiThread

class MainPresenter(private val view: MainView,
                    private val apiRepository: ApiRepository,
                    private val gson: Gson){

    fun getMovieList(){

        doAsync {
            val data = gson.fromJson(apiRepository.doRequest(TMDBApi.getMovie()),
                    MovieResponse::class.java)

            uiThread {
                view.showMovieList(data.results)
            }
        }
    }
}