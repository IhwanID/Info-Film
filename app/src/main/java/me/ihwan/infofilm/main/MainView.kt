package me.ihwan.infofilm.main

import me.ihwan.infofilm.model.Movie

interface MainView{

    fun showMovieList(data: List<Movie>)

}