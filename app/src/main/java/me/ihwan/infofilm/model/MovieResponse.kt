package me.ihwan.infofilm.model

data class MovieResponse(
        val results: List<Movie>
)